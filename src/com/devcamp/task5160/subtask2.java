package com.devcamp.task5160;

import java.util.ArrayList;
import java.util.Random;

public class subtask2 {
    public static void arrayListToArray(){
        Random random = new Random();
        Double[] array = new Double[100];        
        for (int i = 0; i < array.length; i++){
            array[i] = Double.valueOf(random.nextInt());
            // System.out.println(i + 1 + ". " + array[i]);
        }

        ArrayList<Double> list = new ArrayList<>();
        for (Double i : array){
            list.add(i);
        }
        System.out.println(list);
    }
    
    public static void main(String[] args){
        subtask2.arrayListToArray();
    }
}   
