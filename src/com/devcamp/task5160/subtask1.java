package com.devcamp.task5160;

import java.util.ArrayList;
import java.util.Random;

public class subtask1 {
    public static void arrayToArrayList(){
        Random random = new Random();
        Integer[] array = new Integer[100];        
        for (int i = 0; i < array.length; i++){
            array[i] = Integer.valueOf(random.nextInt());
            // System.out.println(i + 1 + ". " + array[i]);
        }

        ArrayList<Integer> list = new ArrayList<>();
        for (Integer i : array){
            list.add(i);
        }
        System.out.println(list);
    }
    
    public static void main(String[] args){
        subtask1.arrayToArrayList();
    }
}
